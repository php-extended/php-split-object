<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-split-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Split;

use RuntimeException;

/**
 * AbstractSplit class file.
 * 
 * This class provides all generic methods for all split strategies.
 * 
 * @author Anastaszor
 */
abstract class AbstractSplit implements SplitInterface
{
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@'.\spl_object_hash($this);
	}
	
	/**
	 * Checks whether the file at given path exists.
	 * 
	 * @param string $filePath
	 * @return boolean
	 * @throws FileNotFoundThrowable
	 */
	protected function checkFileExists(string $filePath) : bool
	{
		if(\is_file($filePath))
		{
			return true;
		}
		
		throw new FileNotFoundException($filePath, 'Failed to find file at '.$filePath);
	}
	
	/**
	 * Checks whether the folder at given.
	 * 
	 * @param string $filePath
	 * @return boolean
	 * @throws FileNotFoundThrowable
	 * @throws FileNotWriteableThrowable
	 */
	protected function checkFileIsWriteable(string $filePath) : bool
	{
		$this->checkFileExists($filePath);
		if(\is_writable($filePath))
		{
			return true;
		}
		
		throw new FileNotWriteableException($filePath, 'File at '.$filePath.' is not writeable.');
	}
	
	/**
	 * Checks whether the files at given paths exists.
	 * 
	 * @param array<integer, string> $filePaths
	 * @return boolean
	 * @throws FileNotFoundThrowable
	 */
	protected function checkFilesExists(array $filePaths) : bool
	{
		foreach($filePaths as $filePath)
		{
			$this->checkFileExists($filePath);
		}
		
		return true;
	}
	
	/**
	 * Checks whether the folder at given path is writeable.
	 * 
	 * @param string $folderPath
	 * @return boolean
	 * @throws FolderNotFoundThrowable
	 * @throws FolderNotWriteableThrowable
	 */
	protected function checkFolderIsWriteable(string $folderPath) : bool
	{
		if(\is_dir($folderPath))
		{
			if(\is_writable($folderPath))
			{
				return true;
			}
			
			throw new FolderNotWriteableException($folderPath, 'Folder at '.$folderPath.' is not writeable.');
		}
		
		throw new FolderNotFoundException($folderPath, 'Failed to find folder at '.$folderPath);
	}
	
	/**
	 * Gets the source file resource for file at given path.
	 * 
	 * @param string $filePath
	 * @return resource
	 * @throws FileNotFoundThrowable
	 * @throws RuntimeException
	 */
	protected function getSourceFileResource(string $filePath)
	{
		$this->checkFileExists($filePath);
		$res = \fopen($filePath, 'r');
		if(false === $res)
		{
			throw new RuntimeException('Failed to open file at '.$filePath);
		}
		
		return $res;
	}
	
	/**
	 * Gets the target file resource for file at given path.
	 * 
	 * @param string $filePath
	 * @return resource
	 * @throws FolderNotFoundThrowable
	 * @throws FolderNotWriteableThrowable
	 * @throws RuntimeException
	 */
	protected function getDestinationFileResource(string $filePath)
	{
		$this->checkFolderIsWriteable(\dirname($filePath));
		$ptr = \fopen($filePath, 'w');
		if(false === $ptr)
		{
			throw new RuntimeException('Failed to open file at '.$filePath);
		}
		
		$res = \flock($ptr, \LOCK_EX);
		if(false === $res)
		{
			\fclose($ptr);
			
			throw new RuntimeException('Failed to lock file at '.$filePath);
		}
		
		return $ptr;
	}
	
	/**
	 *res any file pointer used.
	 * 
	 * @param resource $res
	 * @return boolean
	 */
	protected function releaseFileResource($res) : bool
	{
		\flock($res, \LOCK_UN);
		
		return \fclose($res);
	}
	
}
