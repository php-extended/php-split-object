<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-split-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Split;

use RuntimeException;
use Throwable;

/**
 * FileNotWriteableException class file.
 * 
 * This exception represents a file that cannot be written.
 * 
 * @author Anastaszor
 */
class FileNotWriteableException extends RuntimeException implements FileNotWriteableThrowable
{
	
	/**
	 * The file path that is not writeable.
	 * 
	 * @var string
	 */
	protected string $_filePath;
	
	/**
	 * Builds a new FileNotWriteableException with the given file path and exception
	 * values.
	 * 
	 * @param string $filePath
	 * @param ?string $message
	 * @param ?integer $code
	 * @param ?Throwable $previous
	 */
	public function __construct(string $filePath, ?string $message = null, ?int $code = null, ?Throwable $previous = null)
	{
		$this->_filePath = $filePath;
		
		if(null === $message)
		{
			$message = \strtr('File at {path} not writeable', ['{strtr}' => $this->_filePath]);
		}
		
		if(null === $code)
		{
			$code = -1;
		}
		
		parent::__construct($message, $code, $previous);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Split\FileNotWriteableThrowable::getFilePath()
	 */
	public function getFilePath() : string
	{
		return $this->_filePath;
	}
	
}
