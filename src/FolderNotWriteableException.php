<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-split-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Split;

use RuntimeException;
use Throwable;

/**
 * FolderNotWriteableException class file.
 * 
 * This exception represents a folder that cannot be written in.
 * 
 * @author Anastaszor
 */
class FolderNotWriteableException extends RuntimeException implements FolderNotWriteableThrowable
{
	
	/**
	 * The folder path that is not writeable.
	 * 
	 * @var string
	 */
	protected string $_folderPath;
	
	/**
	 * Builds a new FolderNotWriteableException with the given file path and exception
	 * values.
	 * 
	 * @param string $folderPath
	 * @param ?string $message
	 * @param ?integer $code
	 * @param ?Throwable $previous
	 */
	public function __construct(string $folderPath, ?string $message = null, ?int $code = null, ?Throwable $previous = null)
	{
		$this->_folderPath = $folderPath;
		
		if(null === $message)
		{
			$message = \strtr('Folder at {path} not writeable', ['{strtr}' => $this->_folderPath]);
		}
		
		if(null === $code)
		{
			$code = -1;
		}
		
		parent::__construct($message, $code, $previous);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Split\FolderNotWriteableThrowable::getFolderPath()
	 */
	public function getFolderPath() : string
	{
		return $this->_folderPath;
	}
	
}
