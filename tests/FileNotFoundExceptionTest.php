<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-split-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\Split\FileNotFoundException;
use PHPUnit\Framework\TestCase;

/**
 * FileNotFoundExceptionTest test file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\Split\FileNotFoundException
 *
 * @internal
 *
 * @small
 */
class FileNotFoundExceptionTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var FileNotFoundException
	 */
	protected FileNotFoundException $_object;
	
	public function testToString() : void
	{
		$this->assertStringContainsString(\get_class($this->_object), $this->_object->__toString());
	}
	
	public function testGetFilePath() : void
	{
		$this->assertEquals('/path/to/file', $this->_object->getFilePath());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new FileNotFoundException('/path/to/file');
	}
	
}
