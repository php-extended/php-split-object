<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-split-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\Split\FolderNotWriteableException;
use PHPUnit\Framework\TestCase;

/**
 * FolderNotWriteableExceptionTest test file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\Split\FolderNotWriteableException
 *
 * @internal
 *
 * @small
 */
class FolderNotWriteableExceptionTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var FolderNotWriteableException
	 */
	protected FolderNotWriteableException $_object;
	
	public function testToString() : void
	{
		$this->assertStringContainsString(\get_class($this->_object), $this->_object->__toString());
	}
	
	public function testGetFolderPath() : void
	{
		$this->assertEquals('/path/to/folder', $this->_object->getFolderPath());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new FolderNotWriteableException('/path/to/folder');
	}
	
}
