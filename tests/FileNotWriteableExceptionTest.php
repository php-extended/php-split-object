<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-split-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\Split\FileNotWriteableException;
use PHPUnit\Framework\TestCase;

/**
 * FileNotWriteableExceptionTest test file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\Split\FileNotWriteableException
 *
 * @internal
 *
 * @small
 */
class FileNotWriteableExceptionTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var FileNotWriteableException
	 */
	protected FileNotWriteableException $_object;
	
	public function testToString() : void
	{
		$this->assertStringContainsString(\get_class($this->_object), $this->_object->__toString());
	}
	
	public function testGetFilePath() : void
	{
		$this->assertEquals('/path/to/file', $this->_object->getFilePath());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new FileNotWriteableException('/path/to/file');
	}
	
}
